#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(

    name = "slidedown",
    version = "1.0.0",
    url = 'https://github.com/ondrejsika/slidedown/',
    license = 'GNU LGPL v.3',
    description = "HTML slideshow generator form markdown",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    scripts = ["slidedown"],
    install_requires = ['markdown2'],
    include_package_data = True,
    zip_safe = False,
)

