# slidedown

HTML slideshow generator form markdown

__Author__: Ondrej Sika, <http://ondrejsika.com>, <ondrej@ondrejsika.com>

__GitHub__: <https://github.com/ondrejsika/slidedown>

__PyPI__: <http://pypi.python.org/pypi/slidedown>

__LICENSE__: [MIT](LICENSE)
## Usage

Generate HTML file

```
sika@pc:~$ slidedown slideshow.md > slideshow.html
```

Open slideshow.html in your browser, use left and right arrows for browsing.

Slides are separated by `!SLIDE`.

## Example of source file

```
# Hello

!SLIDE

## This is a exaple slideshow

!SLIDE

## Simple Python code

    for i in xrange(5):
        print 'Hello '

!SLIDE

#### Thanks ...

# Ondrej Sika

* <http://ondrejsika.com>
* <ondrej@ondrejsika.com>
* [@ondrejsika](http://twitter.com/ondrejsika)
```

